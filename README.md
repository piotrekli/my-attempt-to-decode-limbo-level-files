I am trying to decode data files and generate a map of the game world in the game [Limbo](http://playdead.com/games/limbo/) by Playdead.

Current progress:

*   data archives unpacked, files decompressed
*   decoded the low level format of level files
*   made a very simple [map](tree_climbing_map1.png) of a level that does not rely on scripts (`tree_climbing`)

To do:

*   entity [property](#property-types) types, what do "identifiers" mean?
*   bytecode decoding and interpretation (*important!*)
*   texture format
*   level file header format (probably irrelevant)

Archives
========

Limbo uses a simple archive format to store its data. This format only stores
[CRC32](https://en.wikipedia.org/wiki/CRC32) hashes of file names, not the names themselves.
It also does not contain any information like timestamps, modes, directory structure, etc.

The archive file consists of a header containing information needed to locate a file
in the archive, and a data section with file contents.

The format
----------

All numbers are stored as little-endian 32-bit integers.
The header consists of:

*   number of files in the archive
*   for each file:
    *   CRC32 hash of its name
    *   offset of beginning of the file in the archive, *relative to the end of the header (`n_files*12 + 4` bytes)*
    *   size of the file in bytes

List of known file names
------------------------

These names have been discovered mostly by setting a breakpoint on the `crc32` function in gdb
(the `limbo` binary is not stripped!) and reading its argument.

*    [`limbo_mac_boot.pkg`](limbo_mac_boot.pkg.names) (complete except some DirectX shaders)
*    [`limbo_mac_runtime.pkg`](limbo_mac_runtime.pkg.names) (complete)
*    **TODO:** Are the names different on other platforms? `limbo_mac_*` come from the Linux and (I suppose) Mac versions.

Example code
------------

Example code (in C) to extract files from an archive: [unlimbo.c](unlimbo.c)

A Python 2 script to change hashed filenames from an archive to their original (unhashed) form:

```python
#!/usr/bin/python2

from zlib import crc32
import sys, os

def make_hashname(filename):
  return 'hash_%08x'%(crc32(filename)&0xffffffff,)

def replace_file(cn):
  hn = make_hashname(cn)
  if os.access(hn, os.F_OK):
    cn = cn.replace('\\', '/')
    if cn[0] == '/':
      cn = '.' + cn
    dn = os.path.dirname(cn)
    if dn != '' and not os.access(dn, os.F_OK):
      os.makedirs(os.path.dirname(cn))
    os.rename(hn, cn)

if __name__ == '__main__':
  if len(sys.argv) != 3:
    print 'Usage: '+sys.argv[0]+' FILENAME_LIST UNPACKED_ARCHIVE'
    sys.exit(1)
  f = open(sys.argv[1], 'rb')
  names = f.readlines()
  f.close()
  os.chdir(sys.argv[2])
  for n in names:
    if n == '':
      continue
    n = n[:-1]
    if n != '':
      replace_file(n)
```

Directory structure
===================

Most of the files are in the `derived/mac/` directory (**TODO:** is it different on different platforms?).
*If a file in this directory (or its subdirectories) is referenced by another file, its path is relative to `derived/mac/`.*

Files with a `.d` suffix are compressed with "raw" [zlib](https://en.wikipedia.org/wiki/Zlib) compression,
except scripts, which are uncompressed.
*All references to these files (including scripts) omit the `.d` suffix in filenames.*  

Directory structure
-------------------

    - runtime/
      |- data/
      |  `- savepoints/ - text files containing player positions and level filenames
      `- derived/
         `- mac/
            `- data/
               |- savepoints/ - files in the level format
               |- animation/
               |  `- boy/ - contains only `skeleton.branch.d`, purpose unknown
               `- levels/
                  `- _resource/
                     `- camera/ - contains only `mockup_cam.branch.d`
    - boot/
      |- autorun.txt - config file
      |- init.txt - config file for an editor
      |- materials_list.txt - list of materials referenced by the levels, purpose unknown
      |- atlases.txt - list of texture atlases
      |- titledata/
      |  `- bootscreen/ - the loading screen shown when launching the game; images of a 'LOADING' text in different languages
      |- data/
      |  |- texture/
      |  |  `- atlas/ - text files with information about texture coordinates in atlases (?)
      |  |- font/ - fonts in TrueType format
      |  `- shaders/ - DirectX shaders; OpenGL shaders are not in these archives
      `- derived/
         `- mac/
            |- gamesetup.branch.d
            |- usersetup.branch.d
            |- editorsetup.branch.d
            `- data/
               |- animation/ - animations
               |- scripts/ - compiled scripts used by the game, see script format and opcodes
               |  |- test/
               |  |- condition/
               |  |- boy/
               |  |  `- death/
               |  |- tools/
               |  |- ai/
               |  |- mechanisms/
               |  |- physics/
               |  |- animation/
               |  |- camera/
               |  |- sound/
               |  |- render_object/
               |  |- effects/
               |  |- triggers/
               |  |- behaviour/
               |  |- system/
               |  `- levels/
               |- texture/
               |  `- atlas/ - the actual atlas images, see texture format
               |- sprites/ - textures outside the atlasses
               |  |- shapes/
               |  |- props/
               |  |  |- industry/
               |  |  |- various/
               |  |  |- rope/
               |  |  |- nature/
               |  |  `- wires/
               |  |- chapters/ - screenshots in the load chapter menu
               |  |- effects/
               |  |  |- normalmapping/
               |  |  |- debris/
               |  |  |- water_rain/
               |  |  |- light_shadow/
               |  |  `- fire/
               |  |- edges/
               |  |- icons/
               |  |- tests/
               |  `- text/
               |- text/
               `- levels/ - levels, see level format
                  |- _resource/ - varoius props used in the levels
                  |  |- boy/
                  |  |- props/
                  |  |- mechanisms/
                  |  |- emitter_effects/
                  |  |- animals/
                  |  |- screen_effects/
                  |  `- play_menu/
                  `- limbo.scene - the "root" level

File formats:

*   [level format](#branch-level-format)
*   [texture format](#texture-format): **TODO**
*   script format and opcodes: **TODO**

"Branch" (level) format
=======================

File structure
--------------

Limbo level files (with names often ending with `.branch` or `.scene`) use a weird mixed binary and text format.
The file structure is encoded in binary, but all values (including numbers and lists) are saved as text.

Each level file consists of a header and a list of entries.

Binary value types
------------------

The following table contains encodings of several data types used in the format.  
If a value is "typed", it means that it is preceded by a byte with its type ID.

| ID | Type | Encoding |
| -- | ---- | -------- |
| `0x00` | empty/void | has no payload, only seen in the header |
| `0x01` | string | a varint with string `length` followed by a non-zero-terminated ASCII string of `length` bytes |
| `0xFF` | key-value pair | a typed `value`, then a non-typed string `key`; only used in named properties (see below) |
| (none) | varint | one or two bytes: if the first byte (`b0`) has its most significant bit set, it is followed by a second byte (`b1`) and the value is `(b0&0x7f)<<8 \| b1`; otherwise the value is just `b0` |

Header format
-------------

Most of the header fields have unknown purpose, but they are probably not needed to decode a level.
Note that the header has variable size.  
All "short" values probably are little-endian.

| Type | Size (bytes) | Field | Meaning |
| ---- | -----------: | ----- | ------- |
| raw bytes | 8 | magic | file type identifier, always equals `15 00 00 00 AB FD 87 85` |
| unknown | 4 | unknown | is (**TODO:** always?) `00 00 00 00` |
| C string | 33 | persistent ID | an identifier (perhaps a UUID) used in some places in the level files (often preceded by `"0x"` there), saved as a zero-terminated hex string |
| short? | 2 | unknown | (**TODO:** what are the possible values?) |
| short? | 2 | unknown | (**TODO:** what are the possible values?) |
| unknown | 5 | unknown | unknown |
| typed value | variable | comment/name? | the type is usually `0x00` (no data), but can also be a `0x01` (string) |
| short | 2 | unknown | maybe a checksum? |
| short | 2 | unknown | (**TODO:** what are the possible values?) |
| varint | variable | number of entries | the total number of entries (see below) in the file |

Entry serialization format
--------------------------

The header is followed by entries. Each entry represents an object in the game (a level, prop, script, particle system, etc.)
and consists of properties. Keys identifying the properties may be either numbers or strings. Each entry is saved in the level
file as an entry header and a list of properties. The header has the following format:

| Type | Size (bytes) | Field | Meaning |
| ---- | -----------: | ----- | ------- |
| byte | 1 | type | entry type, see below for known types |
| string | variable | name | name identifying the entity, is usually (**TODO:** always?) a number |
| byte | 1 | number of properties | number of properties following the header |

Each property has the following format:

| Type | Size (bytes) | Field | Meaning |
| ---- | -----------: | ----- | ------- |
| byte | 1 | key | the numeric identifier of the property; if it equals `0xFF`, this is a named property and the value field contains a key-value pair (the `0xFF` value type, which *is* present in the file) |
| typed value | variable | value | value of the property (always a string, in case of named properties encapsulated in a key-value pair) |

Known entry types
-----------------

Names found using gdb:

```gdb
set $i = 0
while $i < 0x70
  p *('std::string' *)(('void' *)'ClassType::GetClassType'($i)+0xa8)
  set $i = $i+1
end
```

| ID | Internal type name |
| -- | ------------------ |
| `0x00` | Node |
| `0x01` | Entity |
| `0x02` | Pivot |
| `0x03` | Folder |
| `0x04` | InternalFolder |
| `0x05` | DataBase |
| `0x06` | Branch |
| `0x07` | ContainerBox |
| `0x08` | Layer |
| `0x09` | Camera |
| `0x0A` | RenderObject |
| `0x0B` | RenderTarget |
| `0x0C` | ResourceRef |
| `0x0D` | Mesh |
| `0x0E` | Texture |
| `0x0F` | TextResource |
| `0x10` | MenuController |
| `0x11` | AKObject |
| `0x12` | AKEventBase |
| `0x13` | AKEventTrigger |
| `0x14` | AKEventProxy |
| `0x15` | AKPhysicsTrigger |
| `0x16` | AKPhysicsTriggerImpact |
| `0x17` | AKPhysicsTriggerSlide |
| `0x18` | AKPhysicsTriggerRotate |
| `0x19` | AKPhysicsTriggerRoll |
| `0x1A` | AKPhysicsTriggerMove |
| `0x1B` | AKPhysicsTriggerWaterImpact |
| `0x1C` | AKJointTrigger |
| `0x1D` | AKJointTriggerRotate |
| `0x1E` | AKBoyStateTrigger |
| `0x1F` | AKRTPController |
| `0x20` | AKRTPProxy |
| `0x21` | AKRTPControllerScaledDist |
| `0x22` | AKRTPControllerTwoPoints |
| `0x23` | AKRTPControllerXYBox |
| `0x24` | AKRTPAdsr |
| `0x25` | AKRTPCDef |
| `0x26` | AKListener |
| `0x27` | SoundBankResource |
| `0x28` | SoundBankContainer |
| `0x29` | MovieResource |
| `0x2A` | MovieContainer |
| `0x2B` | BoySoundControl |
| `0x2C` | Sprite |
| `0x2D` | GrassSprite |
| `0x2E` | Water |
| `0x2F` | CollisionRect2D |
| `0x30` | CollisionVolume2D |
| `0x31` | TextPlate |
| `0x32` | ParticleEmitter2 |
| `0x33` | MaterialGroup |
| `0x34` | PostProcess |
| `0x35` | PropertyController |
| `0x36` | Body2D |
| `0x37` | JointPin2D |
| `0x38` | Joint2D |
| `0x39` | JointSlider2D |
| `0x3A` | JointSpring2D |
| `0x3B` | CollisionCircle2D |
| `0x3C` | CollisionPolygon2D |
| `0x3D` | PhysicsWorld2D |
| `0x3E` | BodyCrusher |
| `0x3F` | CellularAutomaton |
| `0x40` | WindSystem2D |
| `0x41` | JointWind2D |
| `0x42` | FloatValue |
| `0x43` | PhysicsTriggerImpact |
| `0x44` | PhysicsTriggerWater |
| `0x45` | Rope |
| `0x46` | AnimationManager |
| `0x47` | AnimationDataNode |
| `0x48` | SkeletonStateNode |
| `0x49` | AnimationEvent |
| `0x4A` | Skeleton |
| `0x4B` | Boy |
| `0x4C` | AnimationNodeState |
| `0x4D` | SkeletonController |
| `0x4E` | Actor |
| `0x4F` | ChildSelectorState |
| `0x50` | RagdollState |
| `0x51` | HierarchyState |
| `0x52` | RandomChildState |
| `0x53` | SequentialChildState |
| `0x54` | NearestChildState |
| `0x55` | BoyRunState |
| `0x56` | BoyPushState |
| `0x57` | BoyJumpState |
| `0x58` | BoyGrabState |
| `0x59` | BoyLadderState |
| `0x5A` | BoyRopeState |
| `0x5B` | BoyReachPlateauState |
| `0x5C` | BoyStandingTurnState |
| `0x5D` | BoySlideState |
| `0x5E` | BoyIdleState |
| `0x5F` | BoyRagdollState |
| `0x60` | PathPivot |
| `0x61` | PathController |
| `0x62` | DebugNode |
| `0x63` | DebugConfig |
| `0x64` | EventMultiplier |
| `0x65` | Profiler |
| `0x66` | EditorCamera |
| `0x67` | EditorDraw |
| `0x68` | Joystick |
| `0x69` | KeyboardInterface |
| `0x6A` | InputConfig |
| `0x6B` | InputCode |
| `0x6C` | Script |

Property types
--------------

All numeric values are stored as text. Vector/list elements, unless noted otherwise, are separated by commas.  
A single property ID can have different meanings depending on the type of the entry it belongs to.

List of properties with numeric IDs (properties appearing only once in the game files are not included):

| ID | Entry types | Format | Description |
| -- | ----------- | ------ | ----------- |
| `0x00` | **TODO** | text | name in editor / comment |
| `0x01` | all | identifier | probably parent ID; this is *not* a number (sometimes contains dots and other symbols) |
| `0x02` | all except `0x49` | identifier | some other type of ID, in the same format as `0x01` |
| `0x03` | all applicable | integer | always(?) 0, 1, 2, 16 or 17 (**TODO:** what are the possible values?) |
| `0x04` | `0x02`, `0x07`, `0x09`, `0x16`, `0x1E`, `0x2D`, `0x32`, `0x39`, `0x3C`, `0x45` | integer | always the same for a given entry type |
| `0x04` | `0x28` | filename | `*.bnk` file |
| `0x04` | `0x2C` | integer | unknown |
| `0x04` | `0x33` | integer | unknown |
| `0x04` | `0x35` | string | names of various parameters, related to physics and rendering |
| `0x04` | `0x3A` | integer | always (?) 2 |
| `0x04` | `0x42` | float | unknown |
| `0x04` | `0x47`, `0x4C` | filename | animation file |
| `0x04` | `0x49` | float | unknown, always a whole number |
| `0x04` | `0x4E`, `0x4F`, `0x50`, `0x51`, `0x52`, `0x53`, `0x5F` | identifier | unknown |
| `0x04` | `0x64` | identifier+ | unknown, some values contain an extra dash and a number |
| `0x04` | `0x6C` | filename | script file |
| `0x05` | `0x02`, `0x06`, `0x07`, `0x08`, `0x09`, `0x13`, `0x14`, `0x16`, `0x17`, `0x18`, `0x1A`, `0x1B`, `0x1D`, `0x1E`, `0x20`, `0x21`, `0x22`, `0x23`, `0x24`, `0x2B`, `0x2C`, `0x2D`, `0x2E`, `0x2F`, `0x31`, `0x32`, `0x34`, `0x36`, `0x37`, `0x39`, `0x3A`, `0x3B`, `0x3C`, `0x3D`, `0x40`, `0x41`, `0x44`, `0x45`, `0x4A`, `0x63`, `0x65`, `0x67` | 3 floats | world position / offset (X, Y, Z?) |
| `0x05` | `0x33` | string | material (see `boot/materials_list.txt`) |
| `0x05` | `0x35` | float | parameter value |
| `0x05` | `0x47` | float | unknown, always a whole number |
| `0x05` | `0x4C` | float | unknown |
| `0x05` | `0x49`, `0x64` | identifier+ | unknown, similar to properties `0x04`, `0x06` and `0x07` of entity type `0x64` |
| `0x05` | `0x50` | float | unknown, always a whole number |
| `0x05` | `0x51`, `0x52`, `0x53` | identifier | unknown |
| `0x05` | `0x5F` | float | unknown, always a multiple of 10 |
| `0x05` | `0x6C` | multi-line text | unknown, looks like VM state dumps, *may be useful for decoding the bytecode* |
| `0x06` | `0x02`, `0x06`, `0x07`, `0x08`, `0x09`, `0x13`, `0x14`, `0x16`, `0x18`, `0x1A`, `0x1D`, `0x1E`, `0x21`, `0x23`, `0x2B`, `0x2C`, `0x2D`, `0x2E`, `0x2F`, `0x31`, `0x32`, `0x36`, `0x37`, `0x39`, `0x3A`, `0x3B`, `0x3C`, `0x3D`, `0x41`, `0x45`, `0x4A`, `0x63`, `0x65`, `0x66`, `0x67` | 3 floats | rotation: the first 2 numbers is the axis, the last one is angle in degrees |
| `0x06` | `0x26` | identifier | unknown |
| `0x06` | `0x47` | float | unknown |
| `0x06` | `0x49` | list of ints | unknown |
| `0x06` | `0x4E` | identifier | unknown |
| `0x06` | `0x50`, `0x5F` | float | unknown, maybe an angle in degrees |
| `0x06` | `0x52`, `0x53` | integer | always 0 |
| `0x06` | `0x64` | identifier+ | unknown, similar to props `0x04`, `0x05` and `0x07` |
| `0x07` | `0x09`, `0x1E`, `0x45` | integer | always 0 |
| `0x07` | `0x2C`, `0x31` | integer | unknown, always 7 or 3 |
| `0x07` | `0x3A` | integer | always (?) 0 |
| `0x07` | `0x47` | float | unknown |
| `0x07` | `0x4C` | float | unknown, always a whole number |
| `0x07` | `0x50`, `0x5F` | float | unknown, always a multiple of 10 |
| `0x07` | `0x51` | float | unknown |
| `0x07` | `0x64` | identifier+ | unknown, similar to props `0x04`, `0x05` and `0x06` |
| `0x08` | `0x02`, `0x06`, `0x07`, `0x09`, `0x10`, `0x14`, `0x1E`, `0x2B`, `0x2C`, `0x2D`, `0x2F`, `0x32`, `0x36`, `0x37`, `0x3A`, `0x3C`, `0x3D`, `0x45`, `0x4A`, `0x63`, `0x65`, `0x66`, `0x6F` | float | unknown, always close to 1 |
| `0x08` | `0x47` | float | unknown |
| `0x08` | `0x4C` | float | always 0 |
| `0x08` | `0x50`, `0x5F` | float | unknown |
| `0x09` | `0x06`, `0x07`, `0x08` | 3 floats | unknown, may be related to position (property `0x05`) |
| `0x09` | `0x09` | 8 hex digits | always FFFFFFFF |
| `0x09` | `0x13`, `0x14`, `0x16`, `0x17`, `0x18`, `0x1a`, `0x1b`, `0x1d`, `0x1e`, `0x20`, `0x21`, `0x22`, `0x23`, `0x24`, `0x2B`, `0x37`, `0x39`, `0x3A` | identifier | unknown |
| `0x09` | `0x2C`, `0x2E`, `0x2F`, `0x32`, `0x3B`, `0x3C` | 6 hex digits | RGB color |
| `0x09` | `0x31`, `0x3D` | 8 hex digits | unknown |
| `0x09` | `0x36` | float | unknown |
| `0x09` | `0x44` | identifier+ | unknown |
| `0x09` | `0x45` | 6 or 8 hex digits | unknown |
| `0x09` | `0x47`, `0x4C` | integer | unknown bitmask |
| `0x09` | `0x5F` | integer | unknown |
| `0x0A` | `0x06`, `0x07`, `0x08` | 3 floats | unknown, possibly related to position (property `0x05`) or `0x09` |
| `0x0A` | `0x09`, `0x2C`, `0x2D`, `0x2E`, `0x2F`, `0x31`, `0x32`, `0x34`, `0x37`, `0x39`, `0x3A`, `0x3B`, `0x3C`, `0x3D`, `0x45` | float | unknown, has only 3 significant digits |
| `0x0A` | `0x14`, `0x22`, `0x23`, `0x24` | integer | always the same for a given entry type |
| `0x0A` | `0x1E` | integer | always 0 |
| `0x0A` | `0x36` | float | unknown |
| `0x0A` | `0x44` | identifier | unknown |
| `0x0A` | `0x4C` | identifier+ | unknown |
| `0x0B` | `0x06` | integer | unknown |
| `0x0B` | `0x07` | integer | unknown bitmask |
| `0x0B` | `0x08` | integer | always 0 |
| `0x0B` | `0x09`, `0x2C`, `0x2E`, `0x2F`, `0x31`, `0x32`, `0x34`, `0x37`, `0x39`, `0x3A`, `0x3B`, `0x3C`, `0x3D`, `0x45` | float | unknown, has only 3 significant digits |
| `0x0B` | `0x20`, `0x21`, `0x22`, `0x23`, `0x24` | string | unknown, always starts with `RTPC_` or `rtpc_` |
| `0x0B` | `0x36` | integer | unknown |
| `0x0B` | `0x4C` | float | unknown |
| `0x0C` | `0x06` | unknown | unknown, may be an identifier or an integer |
| `0x0C` | `0x09`, `0x45` | float | always 0 |
| `0x0C` | `0x13`, `0x14`, `0x16`, `0x17`, `0x18`, `0x1A`, `0x1D`, `0x20`, `0x21`, `0x22`, `0x23` | identifier | unknown |
| `0x0C` | `0x1E`, `0x24` | integer | always 0 |
| `0x0C` | `0x2C` | float | unknown |
| `0x0C` | `0x32`, `0x3A`, `0x3C` | float | unknown |
| `0x0C` | `0x36` | integer | unknown, always 0 or 1 (boolean?) |
| `0x0C` | `0x37`, `0x39` | integer | unknown |
| `0x0C` | `0x4C` | 2 floats | unknown |
| `0x0D` | `0x06` | integer | unknown, has non-zero values in `boot/derived/mac/data/levels/limbo.scene` only |
| `0x0D` | `0x09`, `0x1E`, `0x2D`, `0x31` | integer | always the same for a given entry type |
| `0x0D` | `0x13`, `0x14`, `0x16`, `0x17`, `0x18`, `0x1A`, `0x1D` | integer | unknown, often has values near powers of 2 |
| `0x0D` | `0x2C` | integer | unknown |
| `0x0D` | `0x2E`, `0x2F`, `0x3B`, `0x3C`, `0x45` | integer | unknown, might be a bitmask |
| `0x0D` | `0x32` | integer | unknown |
| `0x0D` | `0x36` | 2 floats | unknown |
| `0x0D` | `0x37`, `0x39` | float | unknown, has only 4 digits after decimal point |
| `0x0D` | `0x3A` | float | unknown |
| `0x0E` | `0x06` | filename | sub-branch file to include |
| `0x0E` | `0x09` | integer | always 0 |
| `0x0E` | `0x13`, `0x21`, `0x22` | identifier | unknown |
| `0x0E` | `0x14` | string | event names, probably related to scripts |
| `0x0E` | `0x1D` | string | event, *starting with `phy_`*, probably related to scripts |
| `0x0E` | `0x1E` | identifier (?) | unknown |
| `0x0E` | `0x20` | float | unknown |
| `0x0E` | `0x23` | 2 floats | unknown |
| `0x0E` | `0x24` | float | always 0.3 |
| `0x0E` | `0x2C` | integer | unknown |
| `0x0E` | `0x31` | integer | always 9 |
| `0x0E` | `0x32` | integer | always? 1 **TODO:** check |
| `0x0E` | `0x36` | float | unknown, has only 4 digits after decimal point |
| `0x0E` | `0x37` | float | unknown, has only 2 digits after decimal point |
| `0x0E` | `0x3A` | float | unknown |
| `0x0E` | `0x45` | integer | always 0 |
| `0x0F` | `0x09` | identifier | always 553 |
| `0x0F` | `0x13`, `0x21`, `0x37` | float | unknown, has only 2 digits after decimal point |
| `0x0F` | `0x14` | integer | unknown, only 2 occurences |
| `0x0F` | `0x16`, `0x17`, `0x18`, `0x1A`, `0x1B` | integer | unknown |
| `0x0F` | `0x1E` | identifier (?) | unknown |
| `0x0F` | `0x22`, `0x23` | float | unknown, always a whole number |
| `0x0F` | `0x2C`, `0x2E`, `0x2F`, `0x31`, `0x32`, `0x34`, `0x3B`, `0x3C`, `0x3D`, `0x45`, `0x4A` | integer | unknown, almost certainly a bitmask |
| `0x0F` | `0x36`, `0x39` | float | unknown, has only 4 digits after decimal point |
| `0x0F` | `0x3A` | float | unknown |
| `0x10` | `0x09` | float | unknown |
| `0x10` | `0x13`, `0x16`, `0x17`, `0x18`, `0x1A`, `0x1B`, `0x1D` | string | event names, probably related to scripts |
| `0x10` | `0x1E` | integer | unknown |
| `0x10` | `0x21` | float | always 100 |
| `0x10` | `0x22` | identifier | unknown |
| `0x10` | `0x23`, `0x24` | float | unknown, always a whole number |
| `0x10` | `0x2C`, `0x2D`, `0x32`, `0x34` | filename | texture |
| `0x10` | `0x2E`, `0x2F`, `0x37`, `0x39`, `0x3C`, `0x45` | float | unknown, has only 4 digits after decimal point |
| `0x10` | `0x31` | filename | font (for in-editor/debug text) |
| `0x10` | `0x36` | float | unknown |
| `0x10` | `0x4A` | integer | always 0 |
| `0x11` | `0x09` | float | always 1 |
| `0x11` | `0x18`, `0x23` | integer | always 1 |
| `0x11` | `0x1E` | float | unknown |
| `0x11` | `0x22` | float | unknown, always 0 or 60 |
| `0x11` | `0x2C`, `0x2D` | 2 floats | unknown |
| `0x11` | `0x2E`, `0x2F`, `0x37`, `0x3B`, `0x3C` | float | unknown, has only 4 digits after decimal point |
| `0x11` | `0x31` | integer | unknown |
| `0x11` | `0x32` | 3 floats | unknown |
| `0x11` | `0x36` | float | unknown |
| `0x11` | `0x45` | integer | unknown |
| `0x11` | `0x4A` | identifier | unknown |
| `0x12` | `0x09` | float | always 0.10 |
| `0x12` | `0x13`, `0x18`, `0x1A` | string | event name (see property `0x10`) |
| `0x12` | `0x16`, `0x1B`, `0x1D` | float | unknown |
| `0x12` | `0x17` | string | event name *starting with `phy_`* |
| `0x12` | `0x1E`, `0x24` | float | unknown |
| `0x12` | `0x23` | identifier | unknown |
| `0x12` | `0x2C`, `0x2D` | 2 floats | unknown |
| `0x12` | `0x2F`, `0x37`, `0x3B`, `0x3C` | float | unknown, has only 4 digits after decimal point |
| `0x12` | `0x31` | integer | always 1 |
| `0x12` | `0x32` | float | unknown |
| `0x12` | `0x34` | 2 floats | unknown |
| `0x12` | `0x45` | filename | texture |
| `0x13` | `0x09` | float | always 10000 |
| `0x13` | `0x16`, `0x1D` | float | unknown |
| `0x13` | `0x1E` | string | event name? |
| `0x13` | `0x2C`, `0x2D` | 2 floats | unknown |
| `0x13` | `0x2F` | float | always -2 |
| `0x13` | `0x31` | string | *in-editor text* (encoded with *Latin-1*), often contains comments in Danish |
| `0x13` | `0x32` | float | unknown |
| `0x13` | `0x34` | 2 floats | unknown |
| `0x13` | `0x37` | float | unknown, probably an angle in degrees |
| `0x13` | `0x3B` | float | always 2 |
| `0x13` | `0x45` | float | unknown |
| `0x14` | `0x09` | 2 floats | unknown |
| `0x14` | `0x13` | identifier | unknown |
| `0x14` | `0x17`, `0x18`, `0x1A` | float | unknown |
| `0x14` | `0x1E`, `0x34` | integer | always 0 |
| `0x14` | `0x2D` | float | unknown |
| `0x14` | `0x31` | 2 floats | unknown |
| `0x14` | `0x32` | float | unknown |
| `0x14` | `0x45`, `0x4A` | integer | unknown |
| `0x15` | `0x09` | float | always 4.0 |
| `0x15` | `0x13` | identifier+ | unknown |
| `0x15` | `0x17`, `0x18`, `0x1A` | float | unknown |
| `0x15` | `0x24`, `0x31` | integer | always 1 |
| `0x15` | `0x2D` | float | unknown |
| `0x15` | `0x2E`, `0x2F` | integer? | unknown **TODO:** maybe an identifier? |
| `0x15` | `0x32` | float | unknown |
| `0x15` | `0x34` | integer | unknown |
| `0x15` | `0x3B`, `0x3C` | integer | unknown bitmask |
| `0x15` | `0x4A` | float | unknown, always a multiple of 100 |
| `0x16` | `0x09` | float | unknown |
| `0x16` | `0x1E`, `0x34` | integer | always 0 |
| `0x16` | `0x24` | integer | always 1 |
| `0x16` | `0x2D` | float | always 0.51 |
| `0x16` | `0x2E`, `0x2F` | integer | unknown |
| `0x16` | `0x31` | float | unknown, has only 1 digit (always 0) after decimal point |
| `0x16` | `0x32` | float | unknown |
| `0x16` | `0x3B`, `0x3C` | integer | unknown |
| `0x16` | `0x4A` | float | unknown, always a multiple of 5 |
| `0x17` | `0x09` | float | unknown, has only 2 digits after decimal point |
| `0x17` | `0x1E` | integer | always 0 |
| `0x17` | `0x2D` | float | unknown |
| `0x17` | `0x2E`, `0x2F`, `0x3B`, `0x3C` | identifier+ | unknown |
| `0x17` | `0x31` | integer | unknown |
| `0x17` | `0x32` | float | unknown |
| `0x17` | `0x34` | integer | always 1 |
| `0x17` | `0x4A` | float | unknown, always a multiple of 100 |
| `0x18` | `0x1E`, `0x31` | integer | always 0 |
| `0x18` | `0x2D` | float | unknown |
| `0x18` | `0x2E`, `0x2F`, `0x3B`, `0x3C` | identifier+ | unknown |
| `0x18` | `0x32` | float | unknown |
| `0x18` | `0x4A` | float | unknown |
| `0x19` | `0x2D` | float | unknown |
| `0x19` | `0x2E`, `0x2F`, `0x3B`, `0x3C` | integer | unknown, always 0 or 1, probably a boolean |
| `0x19` | `0x31` | integer | always 0 |
| `0x19` | `0x32` | float | unknown |
| `0x19` | `0x4A` | float | unknown, always a multiple of 10 |
| `0x1A` | `0x2D`, `0x32` | float | unknown |
| `0x1A` | `0x2E` | integer | always 4 |
| `0x1A` | `0x2F`, `0x3B`, `0x3C` | integer | unknown |
| `0x1A` | `0x4A` | float | unknown, only one entry has a non-zero value (60) |
| `0x1B` | `0x2D` | float | unknown |
| `0x1B` | `0x2E`, `0x2F`, `0x3B`, `0x3C` | string | material (see `boot/materials_list.txt`) |
| `0x1B` | `0x32` | float | unknown |
| `0x1B` | `0x4A` | float | unknown, only one entry has a non-zero value (0.9) |
| `0x1C` | `0x2E`, `0x2F`, `0x3C` | filename | texture |
| `0x1C` | `0x32` | float | unknown |
| `0x1C` | `0x4A` | float | unknown |
| `0x1D` | `0x2E`, `0x2F`, `0x3B`, `0x3C` | float | unknown |
| `0x1D` | `0x32` | float | unknown |
| `0x1E` | `0x2E`, `0x2F`, `0x3B`, `0x3C` | float | unknown |
| `0x1E` | `0x32` | float | unknown |
| `0x1E` | `0x4A` | float | unknown |
| `0x1F` | `0x2D` | float | unknown |
| `0x1F` | `0x2E` | integer | always 2 |
| `0x1F` | `0x32` | float | unknown |
| `0x1F` | `0x4A` | float | unknown |
| `0x20` | `0x2D` | integer | always 1 (?) |
| `0x20` | `0x2E`, `0x2F`, `0x3C` | 2 floats | scale (X, Y), applied before rotation |
| `0x20` | `0x32` | float | unknown |
| `0x20` | `0x3B` | float | unknown |
| `0x20` | `0x4A` | float | unknown |
| `0x21` | `0x2E` | float | always 0.206 |
| `0x21` | `0x32` | float | unknown |
| `0x21` | `0x3B` | integer | unknown |
| `0x21` | `0x3C` | list of float vectors | vertices of object shape polygon, this is a newline-separated list of 2-component vectors, prefixed with a line with the vertex count |
| `0x21` | `0x4A` | float | always 0 |
| `0x22` | `0x2E`, `0x32` | float | unknown |
| `0x23` | `0x2E`, `0x32` | float | unknown |
| `0x24` | `0x2E` | float | unknown |
| `0x24` | `0x32` | integer | unknown |
| `0x25` | `0x2E` | float | unknown |
| `0x25` | `0x32` | integer | unknown bitmask |
| `0x26` | `0x2E`, `0x32` | float | unknown |
| `0x27` | `0x2E`, `0x32` | float | unknown |
| `0x28` | `0x2E`, `0x32` | float | unknown |
| `0x29` | `0x2E`, `0x32` | float | unknown |
| `0x2C` | `0x32` | identifier | unknown |
| `0x2D` | `0x32` | float | always 1 |
| `0x2E` | `0x32` | float | unknown |
| `0x2F` | `0x32` | float | unknown |
| `0x30` | `0x32` | float | unknown |
| `0x31` | `0x32` | identifier | unknown |
| `0x32` | `0x32` | integer | unknown |

List of named properties:

| Name | Entry types | Format | Description |
| ---- | ----------- | ------ | ----------- |
| `foo` | `0x00`, `0x2A` | list of ints | baz qux |

Code
----

Python code for deserializing the data: [deserialize_branch.py](deserialize_branch.py)

Texture format
==============

**TODO**

Random notes
============

*   Limbo has a builtin `printf`-like logging function called `log(char const *, ...)` (`_Z3logPKcz`) which by default
    does nothing, but can be set as breakpoint in a debugger to see the log, however it doesn't seem to contain
    useful information (mostly file names, profiling)
*   The textures are stored in a custom format despite the `.png` suffix. The pixels are saved as raw bytes,
    with grey+alpha for *all* textures in the game.
*   Probably related entry types: (`0x2D`, `0x32`), (`0x2E`, `0x2F`, `0x3B`, `0x3C`)
*   Entries of type `0x4A` often have the same values, except `skeleton.branch/88`, similar with type `0x09`
*   The important properties (for generating a map) of object type `0x3C` are: `0x05` (position), `0x06` (rotation),
    `0x20` (scale), `0x21` (shape)
*   The limbo binary actually contains a script compiler. Unfortunately, it appears to use a hand-written parser.
*   It also contains other things related to scripts, this may be useful:
    ```
    CMD_PUSH_CONST_INT %d
    CMD_PUSH_CONST_FLOAT %f
    CMD_PUSH_CONST_STRING "%s"
    CMD_POP elements=%d
    CMD_DUPLICATE elements=%d
    CMD_PUSH_SELF
    CMD_PUSH_ROOT
    CMD_GET_PARENT
    CMD_LOOKUP_VAR (size=%d)
    CMD_ASSIGN_VAR (size=%d)
    CMD_LOOKUP_EXT_VAR (size=%d)
    CMD_ASSIGN_EXT_VAR (size=%d)
    CMD_LOOKUP_PARAM (size=%d)
    CMD_ASSIGN_PARAM (size=%d)
    CMD_GET_PROPERTY
    CMD_SET_PROPERTY (size=%d)
    CMD_GLOBAL_CALL (id=%d)
    CMD_LOCAL_CALL (address=%d)
    CMD_JUMP offset=%d
    CMD_JUMPIF offset=%d
    CMD_JUMPIFNOT offset=%d
    CMD_GOTO address=%d
    CMD_SLEEPFRAME
    CMD_SLEEP
    CMD_EXIT
    CMD_BREAKPOINT
    CMD_CAST_INT_TO_FLOAT firstcast=%d, casts=%d
    CMD_ARRAY_LOOKUP (arraysize=%d, elementsize=%d)
    CMD_ARRAY_INDEX (elementsize=%d)
    CMD_FUNCLIB_CALL (type=%d,func_id=%d)
    CMD_EXTERNAL_CALL (address=%d, paramsize=%d, returnsize=%d)
    CMD_EXTERNAL_CALL (address UNKNOWN, paramsize=%d, returnsize=%d)
    CMD_CLASS_CALL (function id=%d, paramsize=%d)
    CMD_EXTERNAL_GOTO (paramsize=%d, address=%d)
    CMD_RETURN (paramsize=%d, returnsize=%d)
    ```
    The opcodes are interpreted by a function named `Task::Step`. Next PC is in
    stored in the `[ebp]` register (can be used to determine the number of arguments).

Patches
-------

*   **No death from water and some objects:** `./limbopatch.py -o limbo_mac_runtime.pkg -pzf patches/nodeath1.branch.binpatch data/animation/boy/skeleton.branch.d`

Extra code
----------

**Warning:** this is very bad code

*   [branch-property-types.py](branch-property-types.py)
*   [branch_entity_lister.py](branch_entity_lister.py)
*   [draw_points_from_branch.py](draw_points_from_branch.py)
*   [script-disasm.py](script-disasm.py)
