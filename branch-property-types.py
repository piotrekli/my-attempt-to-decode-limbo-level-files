#!/usr/bin/pypy

from deserialize_branch import deserialize_branch

def vtype(k):
  return k if isinstance(k, basestring) else hex(k)

if __name__ == '__main__':
  import sys, os
  props = {}
  type_props = {}
  for w0 in os.walk(sys.argv[1]):
    for w1 in w0[2]:
      name = w0[0] + '/' + w1
      if not (name.endswith('.branch') or name.endswith('.scene')):
        continue
      f = open(name, 'rb')
      b = deserialize_branch(f)
      f.close()
      for ename, ent in b.iteritems():
        et = ent[type]
        if et not in type_props:
          type_props[et] = set()
        tpk = type_props[et]
        for k, v in ent.iteritems():
          if k == type:
            continue
          if k not in props:
            props[k] = {}
          pk = props[k]
          if et not in pk:
            pk[et] = {}
          pk[et][w1+'/'+ename] = v
          tpk.add(k)
  print 'Found '+`len(type_props)`+' entity types:'
  for k, v in sorted(type_props.iteritems()):
    print '  '+hex(k)+': '+`len(v)`+' properties:'
    print '    '+', '.join(map(vtype, sorted(v)))
  print '\n  ================================== \n'
  print 'Found '+`len(props)`+' property types:'
  for k, v in sorted(props.iteritems()):
    print '\n  ----------------------------------'
    print '  '+vtype(k)+': '+`len(v)`+' types:'
    print '    '+', '.join(map(hex, sorted(v.keys())))
    for et, vals in sorted(v.iteritems()):
      print '    '+hex(et)+': '+`len(vals)`+' occurences:'
      for where, what in sorted(vals.iteritems()):
        print '      '+where.ljust(40)+' : '+`what`
  
