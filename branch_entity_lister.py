#!/usr/bin/pypy

from deserialize_branch import deserialize_branch

def vtype(k):
  return k if isinstance(k, basestring) else '0x%02X'%(k,)

class Property(object):
  def __init__(self, ident, description=None):
    self.ident = ident
    self.description = description
    self.possible_values = {}
    self.occurences = 0
    self.n_objs = 0
  def add_value(self, value):
    if value not in self.possible_values:
      self.possible_values[value] = 0
    self.possible_values[value] += 1
    self.occurences += 1
  def set_n_objs(self, n_objs):
    self.n_objs = n_objs
  def strident(self):
    return vtype(self.ident)
  def __str__(self):
    s = [self.strident() + ':']
    if self.occurences == 0:
      return s[0] + ' does not occur'
    if self.occurences == self.n_objs:
      s.append('present in all instances')
    else:
      s.append('present in %d (%2.02f%%) instances' %
               (self.occurences, 100*self.occurences/float(self.n_objs)))
    if len(self.possible_values) <= 20:
      s.append('possible values: '+', '.join(map(
        lambda x:repr(x[0])+'*'+str(x[1]), sorted(self.possible_values.iteritems()))))
    else:
      s.append('has '+str(len(self.possible_values))+' possible values')
    if self.description != None:
      s.append('desc: '+self.description)
    return '\n  '.join(s)

class EntityType(object):
  def __init__(self, typeid, prop_descriptions):
    self.typeid = typeid
    self.instances = {}
    self.files = {}
    self.numbered_props = {}
    self.named_props = {}
    max_prop_num = -1
    for pdentry in prop_descriptions:
      if pdentry[1] != all and vtype(self.typeid) not in pdentry[1]:
        continue
      key = pdentry[0]
      desc = pdentry[2] + ' - ' + pdentry[3]
      prop = Property(key, desc)
      if isinstance(key, basestring):
        self.named_props[key] = prop
      else:
        self.numbered_props[key] = prop
        max_prop_num = max(max_prop_num, key)
    for i in range(max_prop_num):
      if i not in self.numbered_props:
        self.numbered_props[i] = Property(i)
  def append(self, filename, instname, inst):
    if filename not in self.files:
      self.files[filename] = 0
    self.files[filename] += 1
    self.instances[filename+' :: '+instname] = inst
    for key, value in inst.iteritems():
      if key == type:
        continue
      if not isinstance(key, basestring):
        if key not in self.numbered_props:
          for j in range(key+1):
            if j not in self.numbered_props:
              self.numbered_props[j] = Property(j)
        self.numbered_props[key].add_value(value)
      else:
        if key not in self.named_props:
          self.named_props[key] = Property(key)
        self.named_props[key].add_value(value)
  def finalise(self):
    for prop in self.numbered_props.itervalues():
      prop.set_n_objs(len(self.instances))
    for prop in self.named_props.itervalues():
      prop.set_n_objs(len(self.instances))
  def __str__(self):
    s = ['OBJECT TYPE '+vtype(self.typeid)+
         ': %d instances in %d files'%(len(self.instances), len(self.files))]
    s.append('='*len(s[-1]))
    s.append('\nNUMBERED PROPERTIES:')
    s.append('-'*len(s[-1]))
    for key, prop in sorted(self.numbered_props.iteritems()):
      s.append(str(prop))
    if len(self.named_props) > 0:
      s.append('\nNAMED PROPERTIES:')
      s.append('-'*len(s[-1]))
      for key, prop in sorted(self.named_props.iteritems()):
        s.append(str(prop))
    s.append('\nINSTANCES:')
    s.append('-'*len(s[-1]))
    for where, inst in sorted(self.instances.iteritems()):
      s.append(where+' :')
      for key, value in sorted(inst.iteritems()):
        if key == type:
          continue
        s.append('  '+vtype(key)+': '+repr(value))
    s.append('\nFILES:')
    s.append('-'*len(s[-1]))
    for filename, n_instances in sorted(self.files.iteritems()):
      s.append('  '+filename+' : '+str(n_instances)+' instances')
    s.append('')
    return '\n'.join(s)

class EntityLister(object):
  def __init__(self, prop_descriptions={}):
    self.prop_descriptions = prop_descriptions
    self.objs = {}
  def add_file(self, filename, deserial):
    for name, b in deserial.iteritems():
      self.add_obj(filename, name, b)
  def add_obj(self, filename, instname, b):
    if b[type] not in self.objs:
      self.objs[b[type]] = EntityType(b[type], self.prop_descriptions)
    self.objs[b[type]].append(filename, instname, b)
  def finalise(self):
    for i in self.objs.itervalues():
      i.finalise()

if __name__ == '__main__':
  import sys, os
  
  prop_descriptions = []
  with open(sys.argv[2], 'rb') as f:
    l = f.readlines()
  for i in l:
    i = i.strip()
    if i == '':
      continue
    dl = map(str.strip, i.split('|'))
    prop = dl[0].strip('`')
    if prop.startswith('0x'):
      try:
        prop = int(prop[2:], 16)
      except ValueError:
        pass
    if dl[1] == 'all':
      objtypes = all
    else:
      objtypes = map(lambda x: x.strip().strip('`'), dl[1].split(','))
    prop_descriptions.append((prop, objtypes, dl[2], dl[3]))
  
  lister = EntityLister(prop_descriptions)
  for w0 in os.walk(sys.argv[3]):
    for w1 in w0[2]:
      name = w0[0] + '/' + w1
      if not (name.endswith('.branch') or name.endswith('.scene')):
        continue
      with open(name, 'rb') as f:
        b = deserialize_branch(f)
      lister.add_file(name, b)
  lister.finalise()
  for objtype, objdata in sorted(lister.objs.iteritems()):
    with open(sys.argv[1]+'/type_'+vtype(objtype)+'.txt', 'wb') as f:
      f.write(str(objdata))
  
