#!/usr/bin/python2

import sys

def read_varint(f):
  n = ord(f.read(1))
  if n & 0x80:
    n = (n&0x7f) << 8 | ord(f.read(1))
  return n

def read_none(f):
  return None

def read_string(f):
  n = read_varint(f)
  return f.read(n)

def read_pair(f):
  pairval = read_typed(f)
  pairkey = read_string(f)
  return pairkey, pairval

key_types = {
              0x00: read_none,
              0x01: read_string,
              0xff: read_pair
            }

def read_typed(f):
  ptype = ord(f.read(1))
  if ptype not in key_types:
    raise ValueError(
      'unknown object type '+hex(ptype)+', offset: '+hex(f.tell()-2))
  return key_types[ptype](f)

def deserialize_branch(f):
  branch = {}
  #f.seek(0x0c)
  #branch[id] = f.read(32)
  f.seek(0x36)
  read_typed(f)
  f.read(4)
  declobjs = read_varint(f)
  while True:
    objtype = f.read(1)
    if objtype == '':
      break
    obj = {}
    objtype = ord(objtype)
    objname = read_string(f)
    obj[type] = objtype
    #obj[id] = objname
    nprop = ord(f.read(1))
    for i in xrange(nprop):
      pkey = ord(f.read(1))
      pvalue = read_typed(f)
      if pkey == 0xff and isinstance(pvalue, tuple):
        obj[pvalue[0]] = pvalue[1]
      else:
        obj[pkey] = pvalue
    branch[objname] = obj
  return branch
