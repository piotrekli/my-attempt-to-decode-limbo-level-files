#!/usr/bin/pypy

import difflib, hashlib

def write_varint(val, f):
  varint = []
  while True:
    varint.append((val&0x7f) | 0x80)
    val >>= 7
    if val == 0:
      break
    val -= 1
  varint[0] &= 0x7f
  f.write(bytearray(varint[::-1]))

def bindiff(old, new, out, hash=True):
  sm = difflib.SequenceMatcher(None, old, new, False)
  opcodes = sm.get_opcodes()
  if hash:
    out.write('#')
    out.write(hashlib.md5(old).digest())
  for action, os, oe, ns, ne in opcodes:
    if action == 'equal':
      out.write('=')
      write_varint(oe-os, out)
    if action == 'delete' or action == 'replace':
      out.write('-')
      write_varint(oe-os, out)
    if action == 'insert' or action == 'replace':
      out.write('+')
      write_varint(ne-ns, out)
      out.write(new[ns:ne])

if __name__ == '__main__':
  import sys
  if len(sys.argv) not in (3, 4):
    sys.exit('Usage: '+sys.argv[0]+' OLD NEW [OUTPUT_FILE]')
  with open(sys.argv[1], 'rb') as f:
    old = f.read()
  with open(sys.argv[2], 'rb') as f:
    new = f.read()
  if len(sys.argv) > 3:
    out = open(sys.argv[3], 'wb')
  else:
    out = sys.stdout
  bindiff(old, new, out)
  out.close()
