#!/usr/bin/python2

import struct

def make_arg_proc(t):
  t = '<'+t
  tlen = struct.calcsize(t)
  def arg_proc(x, pc):
    d = []
    for i in xrange(0, len(x), tlen):
      d.extend(map(str, struct.unpack(t, x[i:i+tlen]))[::-1])
    return d
  return arg_proc

def arg_jmp(x, pc):
  off = struct.unpack('<i', x)[0]
  return ['<%04X>' % (pc+2+off,)]

arg_str = lambda x, pc: [repr(x.rstrip('\000'))]
arg_int = make_arg_proc('i')
arg_no = lambda x, pc: []

# this table is not complete and is likely to contain errors
# the argument counts are correct though
opcodes = [
  (1, 'push_const_int', arg_int),
  (1, 'push_const_float', make_arg_proc('f')),
  (str, 'push_const_string', arg_str),
  (1, 'pop', arg_int),
  (1, 'duplicate', arg_int),
  (2, 'cast_int_to_float', arg_int),
  (0, 'push_self', arg_no),
  (0, 'get_parent', arg_no),
  (0, 'push_root', arg_no),
  (2, 'unknown_9', arg_int),
  (1, 'array_index', arg_int),
  (1, 'lookup_foo', arg_int),
  (1, 'assign_foo', arg_int),
  (1, 'unknown_13', arg_int),
  (1, 'unknown_14', arg_int),
  (1, 'unknown_15', arg_int),
  (1, 'unknown_16', arg_int),
  (0, 'get_property', arg_no),
  (1, 'unknown_18', arg_int),
  (1, 'funclib_call', make_arg_proc('hh')),
  (1, 'global_call', arg_int),
  (1, 'local_call', arg_int),
  (3, 'external_call', arg_int),
  (2, 'class_call', arg_int),
  (1, 'jump', arg_jmp),
  (1, 'jumpif', arg_jmp),
  (1, 'jumpifnot', arg_jmp),
  (1, 'call', arg_int),
  (0, 'external_goto', arg_no),
  (2, 'return', arg_int),
  (0, 'out_2', arg_no),
  (0, 'out_3', arg_no),
  (0, 'exit', arg_no),
  (0, '(invalid)', arg_no)
]

if __name__ == '__main__':
  import sys
  data = sys.stdin.read()
  compiler_version, magic, editor_checksum, unknown, n_text_words = \
    struct.unpack('<iIiii', data[:20])
  text = data[20:20+4*n_text_words]
  print 'read '+str(len(data)/4)+' words'
  print 'compiler version: '+str(compiler_version)
  print 'magic: %08x'%(magic,)
  print 'editor checksum: '+str(editor_checksum)
  print '(unknown): '+str(unknown)
  print 'text length: '+str(n_text_words)
  print 'text: '
  pc = 0
  while pc < len(text):
    opc = struct.unpack('<I', text[pc:pc+4])[0]
    if opc > len(opcodes)-2:
      opc = -1
    ilen, iname, iaproc = opcodes[opc]
    if ilen == str:
      n = pc+4
      while text[n] != '\000':
       n += 1
      ilen = (n-pc)/4
    args = text[pc+4:pc+4+ilen*4]
    print '  %04X '%(pc/4,)+iname+' '+', '.join(iaproc(args, pc/4))
    pc += 4*(ilen+1)
