/* This is code to extract files from a Limbo game data archive (limbo_*.pkg)
 * Usage: ./unlimbo LIMBO_PACKAGE OUTPUT_DIR */

#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

typedef /*uint_fast32_t*/ long num_t;

static num_t fgetleu(FILE *f, int bytes)
{
  int i;
  num_t n = 0;
  for (i=0; i<bytes; ++i)
    n |= (num_t)fgetc(f) << i*8;
  return n;
}

static int rmkdir(char *name)
{
  char *sep = name;
  int finish = 0;
  while (1)
  {
    char old;
    sep = strchr(sep, '/');
    if (!sep)
    {
      sep = name + strlen(name) - 1;
      finish = 1;
    }
    sep += 1;
    old = *sep;
    *sep = 0;
    if (mkdir(name, 0755) == -1 && errno != EEXIST)
      return 0;
    if (finish) return 1;
    *sep = old;
  }
}

enum { BUFSIZE = 4096 };

static size_t fcopy(FILE *src, long off, size_t len, FILE *dst)
{
  size_t to_copy = len;
  off_t old_off = ftell(src);
  void *buf = malloc(BUFSIZE);
  if (!buf) return 0;
  if (fseek(src, off, SEEK_SET)) return 0;
  while (to_copy > 0)
  {
    size_t n, count = to_copy > BUFSIZE ? BUFSIZE : to_copy;
    if ((n=fread(buf, 1, count, src)) != count) break;
    if ((n=fwrite(buf, 1, count, dst)) != count)
    {
      to_copy -= n;
      break;
    }
    to_copy -= count;
  }
  free(buf);
  fseek(src, old_off, SEEK_SET);
  return len - to_copy;
}

int unlimbo(FILE *archive)
{
  char name_str[16];
  int n_files, i;
  off_t base_offset;
  n_files = fgetleu(archive, 4);
  base_offset = n_files * 12 + 4;
  if (ferror(archive)) return 0;
  for (i=0; i<n_files && !ferror(archive) && !feof(archive); ++i)
  {
    FILE *f;
    off_t offset, size;
    uint32_t name;
    name = fgetleu(archive, 4);
    offset = fgetleu(archive, 4);
    size = fgetleu(archive, 4);
    if (ferror(archive)) break;
    snprintf(name_str, 15, "hash_%08lx", name&0xffffffffL);
    f = fopen(name_str, "wb");
    if (!f || fcopy(archive, offset+base_offset, size, f) != size) break;
    fclose(f);
  }
  return !ferror(archive) && !feof(archive);
}

int main(int argc, char *argv[])
{
  int rv;
  FILE *f;
  
  if (argc != 3)
  {
    fprintf(stderr, "Usage: %s LIMBO_PACKAGE OUTPUT_DIR\n", argv[0]);
    return 1;
  }
  if (!rmkdir(argv[2])) return 1;
  f = fopen(argv[1], "rb");
  chdir(argv[2]);
  rv = unlimbo(f);
  fclose(f);
  
  return !rv;
}
