#!/usr/bin/pypy

import os, sys

if __name__ == '__main__':
  for w0 in os.walk(sys.argv[1]):
    for w1 in w0[2]:
      name = w0[0] + '/' + w1
      if name.endswith('.d'):
        zname = name[:-2]
      else:
        zname = name+'_decoded'
      with open(name, 'rb') as f:
        contents = f.read()
      try:
        contents = contents.decode('zlib')
        with open(zname, 'wb') as f:
          f.write(contents)
        os.unlink(name)
      except:
        pass
