#!/usr/bin/python2

import sys, cairo
from math import pi, sin, cos
from deserialize_branch import deserialize_branch

branches = {}
branch_name_base = '.'

class ShapeList(object):
  def __init__(self):
    self.shapes = []
    self.min_x = self.min_y = float('inf')
    self.max_x = self.max_y = float('-inf')
  
  def add(self, other):
    self.shapes += other.shapes
    self.metadata += other.metadata
    self.min_x = min(self.min_x, other.min_x)
    self.max_x = max(self.max_x, other.max_x)
    self.min_y = min(self.min_y, other.min_y)
    self.max_y = max(self.max_y, other.max_y)
    return self


class GameEntity(object):
  def __init__(self, branch, objid=None):
    self.branch = branch
    self.objid = objid
    self.obj = None
    if self.objid != None:
      self.obj = branch[objid]
    self.shapes = ShapeList()
    self.off, self.rot, self.scale = get_obj_transform(self.obj)
    self.children = []
    self.texture = None
  
  def init(self):
    if self.objid != None:
      self.init_obj()
    else:
      self.init_branch()
    self.update_children_deps()
  
  def init_obj(self):
    if 0x21 in self.obj:
      self.shapes = load_shape(self.obj[0x21])
    if self.obj[type] == 0x06:
      if 0x0e in self.obj:
        self.children.append(load_branch(self.obj[0x0e]))
      else:
        for n, k in branches.iteritems():
          if k.branch == self.branch:
            print self.objid, n
  
  def init_branch(self):
    for i, obj in self.branch.iteritems():
      if (0x21 in obj and obj[type] in (0x3c,)) or obj[type] == 0x06:
        ent = GameEntity(self.branch, i)
        ent.init()
        self.children.append(ent)
  
  def update_children_deps(self):
    for i in self.children:
      self.shapes.min_x = min(self.shapes.min_x, i.off[0]+i.shapes.min_x)
      self.shapes.max_x = max(self.shapes.max_x, i.off[0]+i.shapes.max_x)
      self.shapes.min_y = min(self.shapes.min_y, i.off[1]+i.shapes.min_y)
      self.shapes.max_y = max(self.shapes.max_y, i.off[1]+i.shapes.max_y)
  
  def draw(self, cr, crmat):
    cr.save()
    cr.translate(self.off[0]+self.rot[0], self.off[1]+self.rot[1])
    cr.rotate(self.rot[2]*(-pi/180.0))
    cr.translate(-self.rot[0], -self.rot[1])
    cr.scale(self.scale[0], self.scale[1])
    for i in xrange(len(self.shapes.shapes)):
      sh = self.shapes.shapes[i]
      n = i/float(len(self.shapes.shapes))
      cr.set_source_rgba(1-n, 0.7, n, 0.25)
      if 0x1c in self.obj:
        cr.set_source_rgba(0.5, 1.0, 0.8, 0.25)
      cr.move_to(*sh[-1])
      for xy in sh:
        cr.line_to(*xy)
      cr.close_path()
      cr.save()
      cr.set_matrix(crmat)
      cr.fill_preserve()
      cr.set_source_rgba(1-n, 0.4, n, 1.0)
      cr.stroke()
      cr.restore()
    if self.objid != None:
      cr.set_source_rgb(1.0, 1.0, 1.0)
      cr.move_to(0, 0)
      cr.save()
      cr.set_matrix(crmat)
      cr.show_text(self.objid)
      cr.restore()
    for child in self.children:
      #print child.scale
      child.draw(cr, crmat)
    cr.restore()


def parse_float_list(s):
  return tuple(map(lambda x: float(x.strip()), s.split(',')))

def rotate(pos, angle):
  c, s = cos(angle), sin(angle)
  return (pos[0]*c-pos[1]*s, pos[0]*s+pos[1]*c)

def load_shape(s):
  l = s.splitlines()
  sl = ShapeList()
  p = []
  for i in xrange(len(l)):
    if len(l[i].strip()) == 0 and len(p) > 0:
      sl.shapes.append(p)
      p = []
      continue
    j = parse_float_list(l[i])
    if len(j) != 2:
      continue
    #j = rotate((j[0]*scale[0]-rot[0], j[1]*scale[1]-rot[1]), -rot[2]*(pi/180.0))
    #j = (j[0]+rot[0]+off[0], -j[1]-rot[1]-off[1])
    p.append(j)
    sl.min_x = min(sl.min_x, j[0])
    sl.max_x = max(sl.max_x, j[0])
    sl.min_y = min(sl.min_y, j[1])
    sl.max_y = max(sl.max_y, j[1])
  if len(p) > 0:
    sl.shapes.append(p)
  return sl

def print_obj(i, obj):
  print 'Obj(0x%02x): "%s"' % (obj[type], i)
  for key, value in obj.iteritems():
    if key == type:
      break
    if isinstance(key, int):
      key = 'prop_0x%02x' % (key,)
    print '  %s = %s' % (key, repr(value))
  print

def get_obj_transform(obj):
  off = (0.0, 0.0)
  rot = (0.0, 0.0, 0.0)
  scale = (1.0, 1.0)
  if obj != None:
    if 0x05 in obj:
      off = parse_float_list(obj[0x05])
    if 0x06 in obj:
      rot = parse_float_list(obj[0x06])
    if 0x20 in obj:
      scale = parse_float_list(obj[0x20])
      if len(scale) != 2:
        print hex(obj[type]), scale
        scale = (1.0, 1.0)
  return off, rot, scale

def load_branch(name):
  if name in branches:
    return branches[name]
  path = branch_name_base + '/' + name
  with open(path, 'rb') as f:
    bdata = deserialize_branch(f)
  entity = GameEntity(bdata)
  branches[name] = entity
  entity.init()
  return entity

if __name__ == '__main__':
  branch_name_base = sys.argv[1]
  ent = load_branch(sys.argv[2])
  orig = ((ent.shapes.min_x+ent.shapes.max_x)*0.5, (ent.shapes.min_y+ent.shapes.max_y)*0.5)
  zoom = max(ent.shapes.max_x-ent.shapes.min_x, ent.shapes.max_y-ent.shapes.min_y)
  
  w, h, scale, lscale = 2048, 2048, 0.9/zoom, 4.0
  scale /= lscale
  sfc = cairo.ImageSurface(cairo.FORMAT_ARGB32, w, h)
  cr = cairo.Context(sfc)
  cr.translate(w/2., h/2.)
  crmat = cr.get_matrix()
  cr.scale(lscale, -lscale)
  cr.translate(-orig[0]*scale*w, -orig[1]*scale*h)
  cr.scale(w*scale, h*scale)
  cr.set_line_width(1.0)
  cr.set_font_size(10.0)
  cr.set_source_rgb(0.2, 0.2, 0.2)
  cr.paint()
  
  ent.draw(cr, crmat)
  
  cr.set_source_rgb(1.0, 0.0, 1.0)
  cr.new_path()
  #cr.arc(0.0, 0.0, 1.0, 0.0, 2*pi)
  #cr.stroke()
  cr.arc(0.0, 0.0, 0.1, 0.0, 2*pi)
  cr.fill()
  sfc.write_to_png('out.png')
