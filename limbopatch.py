#!/usr/bin/pypy

import struct, zlib, hashlib
from cStringIO import StringIO

PM_DELETE     = 1
PM_COMPRESSED = 2
PM_PATCH      = 4

NAME_PREFIX = 'derived/mac/'

def read_varint(f):
  val = -1
  while True:
    c = ord(f.read(1))
    val = ((val+1)<<7) | (c&0x7f)
    if (c&0x80) == 0:
      break
  return val

def binpatch(oldf, patch):
  newf = StringIO()
  while True:
    cmd = patch.read(1)
    if cmd == '':
      break
    elif cmd == '=':
      size = read_varint(patch)
      newf.write(oldf.read(size))
    elif cmd == '-':
      oldf.seek(read_varint(patch), 1)
    elif cmd == '+':
      size = read_varint(patch)
      newf.write(patch.read(size))
    elif cmd == ';':
      size = read_varint(patch)
      patch.seek(size)
    elif cmd == '#':
      md5 = patch.read(16)
      pos = oldf.tell()
      oldf.seek(0)
      old_md5 = hashlib.md5(oldf.read()).digest()
      oldf.seek(pos)
      if md5 != old_md5:
        raise ValueError('Binary patch input is incorrect (already patched?)')
    else:
      raise ValueError('Invalid patch command: '+repr(cmd))
  return newf.getvalue()

class LimboArchive(object):
  def __init__(self, f):
    self.files = []
    if f != None:
      nfiles = struct.unpack('<I', f[0:4])[0]
      for i in xrange(nfiles):
        namehash, off, size = struct.unpack('<iII', f[4+i*12:4+i*12+12])
        off += nfiles*12+4
        self.files.append((namehash, f[off:off+size]))
  
  def patch(self, filename, mode, input):
    idx = 0
    possible_hashes = map(zlib.crc32, [filename, NAME_PREFIX+filename])
    for namehash, data in self.files:
      if namehash in possible_hashes:
        break
      idx += 1
    else:
      if mode & PM_DELETE:
        return
      self.files.append((possible_hashes[-1], ''))
    if mode & PM_DELETE:
      self.files.pop(idx)
      return
    if mode & PM_PATCH:
      old = self.files[idx][1]
      if mode & PM_COMPRESSED:
        old = zlib.decompress(old)
      input = binpatch(StringIO(old), StringIO(input))
    if mode & PM_COMPRESSED:
      input = zlib.compress(input)
    self.files[idx] = (self.files[idx][0], input)
  
  def save(self, out):
    dataptr = 0
    out.write(struct.pack('<I', len(self.files)))
    for namehash, data in self.files:
      size = len(data)
      out.write(struct.pack('<iII', namehash, dataptr, size))
      dataptr += size
    for namehash, data in self.files:
      out.write(data)

if __name__ == '__main__':
  import sys, argparse, os
  
  class FileParseAction(argparse.Action):
    def __call__(self, parser, namespace, values, option_string=None):
      if option_string == None:
        #print values
        filespec = (values, namespace.P_infile, namespace.P_flags)
        namespace.P_infile = None
        namespace.P_flags = 0
        getattr(namespace, self.dest).append(filespec)
      else:
        setattr(namespace, self.dest,
                getattr(namespace, self.dest) | self.const)
  
  ap = argparse.ArgumentParser(description='Patch a Limbo game assets archive',
                               fromfile_prefix_chars='@')

  ap.add_argument('--prefix', '-P', type=str, default=NAME_PREFIX,
                  help='global filename prefix')
  ap.add_argument('--input', '-i', type=str, default=None,
                  help='input archive file')
  ap.add_argument('--output', '-o', type=str, default=None,
                  help='output archive file')
  
  ag = ap.add_argument_group('per-file options')
  ag.add_argument('--file', '-f', type=str,
                  dest='P_infile', metavar='FILE',
                  help='input filename in filesystem')
  ag.add_argument('--delete', '-d', action=FileParseAction, type=int,
                  nargs=0, default=0, const=PM_DELETE, dest='P_flags',
                  help='remove the file from the archive')
  ag.add_argument('--compressed', '-z', action=FileParseAction, type=int,
                  nargs=0, default=0, const=PM_COMPRESSED, dest='P_flags',
                  help='compress the file with zlib')
  ag.add_argument('--patch', '-p', action=FileParseAction, type=int,
                  nargs=0, default=0, const=PM_PATCH, dest='P_flags',
                  help='patch the file instead of replacing')
  ag.add_argument('patches', metavar='FILES', action=FileParseAction,
                  nargs='?', help='filename in archive', default=[])
  ag.add_argument('remainder', nargs=argparse.REMAINDER,
                  help=argparse.SUPPRESS)
  
  ns = ap.parse_args()
  while ns.remainder != []:
    ns = ap.parse_args(ns.remainder, ns)
  
  if ns.output == None:
    sys.exit('error: You must specify the input and output file.')
  if ns.input == None:
    ns.input = ns.output
  NAME_PREFIX = ns.prefix
  
  with open(ns.input, 'rb') as f:
    a = LimboArchive(f.read())
  for ofn, ifn, mode in ns.patches:
    if ifn == None:
      ifn = ofn
    if mode & PM_DELETE:
      data = None
    else:
      with open(ifn, 'rb') as f:
        data = f.read()
    a.patch(ofn, mode, data)
  with open(ns.output, 'wb') as output:
    a.save(output)
