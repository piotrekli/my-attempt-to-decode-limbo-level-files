#!/usr/bin/python2

"""
The names can be extracted using gdb (on Linux x86) using:

  set $id = 0
  while $id < 0x70
    set $cls = (void *)'ClassType::GetClassType'($id)
    set $nprop = *(void ***)($cls+0x4)-*(void ***)($cls+0x0)
    set $i = 0
    p $id
    p (*('std::string' *)($cls+0xa8)).c_str()
    set $parent = (void *)*(int *)($cls+0x78)
    if $parent == 0
      p "<NULL>"
    else
      p (*('std::string' *)($parent+0xa8)).c_str()
    end
    while $i < $nprop
      p (char*)*(void **)(*(void **)(*(void **)($cls+0x0)+4*$i)+0x8)
      set $i = $i+1
    end
    set $id = $id+1
  end

The gdb output has to be preprocessed by removing the beginning of each line:
  ^\$[0-9]* = (0x[0-9a-f]* )?
"""

class Type(object):
  def __init__(self, name, parent, props):
    self.name = name
    self.parent = parent
    self.props = props

def lineiter(lines):
  to_yield = []
  for l in lines:
    l = l.strip()
    if l == '':
      continue
    if l[0] == '"':
      to_yield.append(l[1:-1])
    else:
      if to_yield != []:
        yield to_yield
      to_yield = []

def print_type(t, types, i=0):
  if t.parent != '<NULL>':
    i = print_type(types[t.parent], types, i)
  print ' -- ' + t.name + ' --'
  for j in t.props:
    print '  0x%02x: %s' % (i, j)
    i += 1
  return i

if __name__ == '__main__':
  import sys
  lines = sys.stdin.readlines()
  types = {}
  typenames = []
  for g in lineiter(lines):
    types[g[0]] = Type(g[0], g[1], g[2:])
    typenames.append(g[0])
  for tn in typenames:
    print tn
    print_type(types[tn], types)
    print
